module gitee.com/eden-framework/plugin-event

go 1.14

require (
	gitee.com/eden-framework/enumeration v1.0.1
	gitee.com/eden-framework/plugins v0.0.7
	github.com/derekparker/trie v0.0.0-20200317170641-1fdf38b7b0e9
	github.com/profzone/data_structures v0.0.0-20181101025750-3261ce190878
)
